This archive contains the C++ source code for an FPGA-implementation of the Inferior Olive-model, for a network of 10 cells with custom inputs and connectivity matrix. The implementation should fit a ZedBoard and uses one kernel to compute the state of each cell in series, across time steps.

The testbench-code (infoli_tb.cpp, requires infoli.h) can be executed:
- Without any arguments, in which case a full connection matrix and a one-pulse input will be given
- With the input file as an argument, in which case a full connection matrix will be used, and the inputs are read from the input file (one line per timestep).
- With the input file and the connection matrix file (in that order) as arguments.

Sample input files (inputFile.txt and connectionFile.txt) are included. The testbench gives the axon voltage of each cell as output, writing to InferiorOlive_Output.txt.

The file infoli.cpp (requires infoli.h) contains the ComputeNetwork function, which is to be synthesized. directives.tcl contains the Vivado HLS directives. The code was tested for C/RTL cosimulation in Vivado HLS 2017.3 with the supplied input files and ran without errors.
Developer by Georgios Smaragdos and validated by Jan-Harm Betting on Behalf of Neurasmus B.V. 
